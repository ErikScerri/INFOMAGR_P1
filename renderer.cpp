#define TINYOBJLOADER_IMPLEMENTATION
#define USE_BVH

#include "precomp.h" // include (only) this in every .cpp file

inline float deg2rad(const float &deg) { return deg * M_PI / 180; }

inline Pixel clr2pix(const Color &color)
{
	const unsigned r = (uint32_t)(255 * CLAMP(color.x, 0, 1)) << 16;
	const unsigned g = (uint32_t)(255 * CLAMP(color.y, 0, 1)) << 8;
	const unsigned b = (uint32_t)(255 * CLAMP(color.z, 0, 1));

	return r + g + b;
}

mat4 Renderer::SetCameraMatrix(const Camera &camera)
{
	mat4 cameraToWorld = mat4();

	cameraToWorld = cameraToWorld.rotatexyz(camera.orient.x, camera.orient.y, camera.orient.z);

	vec3 dirx = transformvector(vec3(1, 0, 0), cameraToWorld);
	vec3 diry = transformvector(vec3(0, 1, 0), cameraToWorld);
	vec3 dirz = transformvector(vec3(0, 0, 1), cameraToWorld);

	vec3 dir = (dirx * camera.pos.x) + (diry * camera.pos.y) + (dirz * camera.pos.z);

	cameraToWorld.cell[3] = dir.x;
	cameraToWorld.cell[7] = dir.y;
	cameraToWorld.cell[11] = dir.z;

	return cameraToWorld;
}

void Renderer::Render(const Camera &camera, const std::vector<std::unique_ptr<Object>> &objects, const std::vector<std::unique_ptr<Light>> &lights, const std::vector<std::unique_ptr<BVH>> &bvhs, Surface* screen)
{
	mat4 cameraToWorld = SetCameraMatrix(camera);

	float scale = tan(deg2rad(camera.fov * 0.5));
	float aspectratio = camera.w / (float)camera.h;

	vec3 orig = transformpoint(vec3(0), cameraToWorld);

	clock_t start, end;
	start = clock();

	for (int j = 0; j < camera.h; j++) for (int i = 0; i < camera.w; i++)
	{
		if (j == 355 && i == 355)
		{
			int test = 0;
		}
		
		float x = (2 * (i + 0.5) / (float)camera.w - 1) * aspectratio * scale;
		float y = (1 - 2 * (j + 0.5) / (float)camera.h) * scale;

		vec3 dir = transformvector(vec3(x, y, -1), cameraToWorld);
		dir.normalize();

		Ray ray = Ray(orig, dir, PRIMARY);
		Color result = Trace(ray, objects, lights, bvhs, camera, 0);
		screen->Plot(i, j, clr2pix(result));
	}

	end = clock();

	std::cout << "Render complete: " << (double)(end - start) / CLOCKS_PER_SEC << " seconds" << std::endl;
}

Color Renderer::Trace(Ray &ray, const std::vector<std::unique_ptr<Object>> &objects, const std::vector<std::unique_ptr<Light>> &lights, const std::vector<std::unique_ptr<BVH>> &bvhs, const Camera &camera, uint32_t depth)
{
	if (depth > MAX_DEPTH) return camera.background;

	Color hitColor = 0;
	uint32_t collidx = 0;
	
#ifdef USE_BVH
	//Inverse ray for Bounding Box intersection (pre-calculated to avoid multiple calculations)
	vec3 invdir = vec3(1 / ray.dir.x, 1 / ray.dir.y, 1 / ray.dir.z);
	int sign[3] = { invdir.x < 0, invdir.y < 0, invdir.z < 0 };
	Ray invray = Ray(ray.orig, invdir, ray.type);

	//Determines if previous BVH has resulted in an intersection already
	bool prevcoll = false;

	//Same process of intersection but goes through all BVHs
	for (int i = 0; i < bvhs.size(); i++)
	{
		BVH* currBVH = bvhs[i].get();
		Object* hitObj = objects[currBVH->objidx].get();

		if (TraverseBVH(ray, invray, sign, currBVH, currBVH->root, hitObj, collidx))
		{
			prevcoll = true;

			Material hitMat = hitObj->GetMaterial(collidx);
		
			vec3 hitPoint = ray.orig + ray.dir * ray.t;
			vec3 hitNorm;
			hitObj->GetSurfaceData(hitPoint, hitNorm, collidx);

			switch (hitMat.type)
			{
				case DIFFUSE:
				{
					hitColor = DirectIllumination(ray, lights, objects, bvhs, hitNorm, hitMat);
					break;
				}
				case MIRROR:
				{
					vec3 reflDir = Reflect(ray.dir, hitNorm);
					vec3 reflOrig = hitPoint + hitNorm * SHADOWBIAS;
					Ray reflRay = Ray(reflOrig, reflDir);
					hitColor = 0.8 * hitMat.color * Trace(reflRay, objects, lights, bvhs, camera, depth + 1);
					break;
				}
				case GLASS:
				{
					Color refrColor, reflColor;

					float kr;
					Fresnel(ray.dir, hitNorm, hitMat.idxofref, kr);

					bool outside = dot(ray.dir, hitNorm) < 0;
					vec3 bias = SHADOWBIAS * hitNorm;

					if (kr < 1)
					{
						vec3 refrDir = Refract(ray.dir, hitNorm, hitMat.idxofref).normalized();
						vec3 refrOrig = outside ? hitPoint - bias : hitPoint + bias;
						Ray refrRay = Ray(refrOrig, refrDir);
						refrColor = Trace(refrRay, objects, lights, bvhs, camera, depth + 1);
					}

					vec3 reflDir = Reflect(ray.dir, hitNorm).normalized();
					vec3 reflOrig = outside ? hitPoint + bias : hitPoint - bias;				
					Ray reflRay = Ray(reflOrig, reflDir);
					reflColor = hitMat.color * Trace(reflRay, objects, lights, bvhs, camera, depth + 1);

					hitColor += reflColor * kr + refrColor * (1 - kr);
					break;
				}
				default:
					break;
			}
		}
		else
		{
			if (!prevcoll) hitColor = camera.background;
		}
	}
#else
	const Object* hitObj = nullptr;

	if (NearestIntersection(ray, objects, hitObj, collidx))
	{
		Material hitMat = hitObj->GetMaterial(collidx);

		vec3 hitPoint = ray.orig + ray.dir * ray.t;
		vec3 hitNorm;
		hitObj->GetSurfaceData(hitPoint, hitNorm, collidx);

		switch (hitMat.type)
		{
		case DIFFUSE:
		{
			hitColor = DirectIllumination(ray, lights, objects, bvhs, hitNorm, hitMat);
			break;
		}
		case MIRROR:
		{
			vec3 reflDir = Reflect(ray.dir, hitNorm);
			vec3 reflOrig = hitPoint + hitNorm * SHADOWBIAS;
			Ray reflRay = Ray(reflOrig, reflDir);
			hitColor = 0.8 * hitMat.color * Trace(reflRay, objects, lights, bvhs, camera, depth + 1);
			break;
		}
		case GLASS:
		{
			Color refrColor, reflColor;

			float kr;
			Fresnel(ray.dir, hitNorm, hitMat.idxofref, kr);

			bool outside = dot(ray.dir, hitNorm) < 0;
			vec3 bias = SHADOWBIAS * hitNorm;

			if (kr < 1)
			{
				vec3 refrDir = Refract(ray.dir, hitNorm, hitMat.idxofref).normalized();
				vec3 refrOrig = outside ? hitPoint - bias : hitPoint + bias;
				Ray refrRay = Ray(refrOrig, refrDir);
				refrColor = Trace(refrRay, objects, lights, bvhs, camera, depth + 1);
			}

			vec3 reflDir = Reflect(ray.dir, hitNorm).normalized();
			vec3 reflOrig = outside ? hitPoint + bias : hitPoint - bias;
			Ray reflRay = Ray(reflOrig, reflDir);
			reflColor = hitMat.color * Trace(reflRay, objects, lights, bvhs, camera, depth + 1);

			hitColor += reflColor * kr + refrColor * (1 - kr);
			break;
		}
		default:
			break;
		}
	}
	else
	{
		hitColor = camera.background;
	}
#endif

	return hitColor;
}

bool Renderer::TraverseBVH(Ray &ray, Ray &invray, int* sign, BVH* bvh, BVHNode &node, Object* &hitObj, uint32_t &collidx)
{
	//If the biggest bounding box doesn't intersect, no smaller ones will
	if (!node.bbox.Intersect(invray, sign)) return false;

	//If the number of primitives is less than the minimum number, node is a leaf
	if (node.count < LEAFCOUNT)
	{
		return NearestIntersection(ray, hitObj, node.left_first, node.count, collidx);
	}
	else
	{
		//Determine which side of the tree (left or right) is closest to ray origin, traverse that first
		vec3 vecToLeft = ray.orig - bvh->pool[node.left_first].bbox.Centroid().tovec3();
		vec3 vecToRight = ray.orig - bvh->pool[node.left_first + 1].bbox.Centroid().tovec3();

		float distToLeft = vecToLeft.sqrLength();
		float distToRight = vecToRight.sqrLength();

		if (distToLeft < distToRight)
		{
			bool left = TraverseBVH(ray, invray, sign, bvh, bvh->pool[node.left_first], hitObj, collidx);
			bool right = TraverseBVH(ray, invray, sign, bvh, bvh->pool[node.left_first + 1], hitObj, collidx);
			return left || right;
		}
		else
		{
			bool left = TraverseBVH(ray, invray, sign, bvh, bvh->pool[node.left_first + 1], hitObj, collidx);
			bool right = TraverseBVH(ray, invray, sign, bvh, bvh->pool[node.left_first], hitObj, collidx);
			return left || right;
		}

	}
}

bool Renderer::NearestIntersection(Ray &ray, Object* &hitObj, const int &first, const int &count, uint32_t &collidx)
{
	if (ray.type == SHADOW && hitObj->GetMaterial(collidx).type == GLASS) return false;
	Ray rayTMP = Ray(ray.orig, ray.dir, ray.type);
	uint32_t collidxTMP = 0;
	if (hitObj->Intersect(rayTMP, collidxTMP, first, count) && rayTMP.t < ray.t)
	{
		ray.t = rayTMP.t;
		collidx = collidxTMP;
		return true;
	}
	return false;
}

bool Renderer::NearestIntersection(Ray &ray, const std::vector<std::unique_ptr<Object>> &objects, const Object* &hitObj, uint32_t &collidx)
{	
	for (int i = 0; i < objects.size(); i++)
	{
		if (ray.type == SHADOW && objects[i]->GetMaterial(collidx).type == GLASS) continue;
		Ray rayTMP = Ray(ray.orig, ray.dir, ray.type);
		uint32_t collidxTMP = 0;
		if (objects[i]->Intersect(rayTMP, collidxTMP) && rayTMP.t < ray.t)
		{
			hitObj = objects[i].get();
			ray.t = rayTMP.t;
			collidx = collidxTMP;
		}			
	}

	return (hitObj != nullptr);
}

Color Renderer::DirectIllumination(Ray &ray, const std::vector<std::unique_ptr<Light>> &lights, const std::vector<std::unique_ptr<Object>> &objects, const std::vector<std::unique_ptr<BVH>> &bvhs, const vec3 &hitNorm, const Material &hitMat)
{
	Color diffuseColor = 0, specularColor = 0;

	vec3 hitPoint = ray.orig + ray.dir * ray.t;

	for (int i = 0; i < lights.size(); i++)
	{
		vec3 lightDir;
		Color lightColor;
		float tShadow;
		uint32_t collidx = 0;

		lights[i]->Illuminate(hitPoint, lightDir, lightColor, tShadow);

		const Object* shadowObj = nullptr;
		Ray shadowRay = Ray(hitPoint + hitNorm * SHADOWBIAS, -lightDir, SHADOW);

#ifdef USE_BVH
		bool vis = true;

		vec3 invdir = vec3(1 / shadowRay.dir.x, 1 / shadowRay.dir.y, 1 / shadowRay.dir.z);
		int sign[3] = { invdir.x < 0, invdir.y < 0, invdir.z < 0 };
		Ray invray = Ray(shadowRay.orig, invdir, shadowRay.type);

		for (int i = 0; i < bvhs.size(); i++)
		{
			BVH* currBVH = bvhs[i].get();
			Object* hitObj = objects[currBVH->objidx].get();

			vis = vis && !TraverseBVH(shadowRay, invray, sign, currBVH, currBVH->root, hitObj, collidx);
		}
#else
		bool vis = !NearestIntersection(shadowRay, objects, shadowObj, collidx);
#endif

		float LdotN = MAX(0.f, dot(-lightDir, hitNorm));

		diffuseColor += vis * hitMat.albedo * lightColor * LdotN;

		vec3 reflDir = Reflect(lightDir, hitNorm);
		specularColor += powf(MAX(0.f, -dot(reflDir, ray.dir)), hitMat.specExp) * lightColor * vis;
	}

	return diffuseColor * hitMat.color * hitMat.ratioD + specularColor * hitMat.ratioS;
}

bool Renderer::LoadObjFile(const char* objfile, const char* mtlfile, bool triangulate, ObjMesh &obj)
{
	std::cout << "Loading " << objfile << "..." << std::endl;

	std::string err;
	bool ret = tinyobj::LoadObj(&obj.attrib, &obj.shapes, &obj.materials, &err, objfile, mtlfile, triangulate);

	if (!err.empty()) {
		std::cerr << err << std::endl;
	}

	if (!ret) {
		printf("Failed to load/parse .obj.\n");
		return false;
	}

	return true;
}